#pragma once
#include <stdint.h>

typedef uint8_t CRCType;

#define CRC_16_ARC 0x8005
#define CRC_8_CCITT 0x07

#define WIDTH  (8 * sizeof(CRCType))
#define TOPBIT (1 << (WIDTH - 1))

CRCType GetCRC(void* input, int sizeInBytes);