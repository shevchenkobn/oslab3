#include "stdafx.h"
#include "EmailManager.h"


EmailManager::EmailManager() : EmailManager((TCHAR*)_T("D:\\"))
{
}

EmailManager::EmailManager(TCHAR * rootPath)
{
	InitiateDirectory(rootPath);
	currentMailbox = nullptr;
}

EmailManager::~EmailManager()
{
}

EmailManagerError EmailManager::CreateMailbox(TCHAR* login)
{
	// File structure 32bit sizeOfFile - 32bit numberofMes - 32bit MaxMes - 8bit checksum - messages
	int size = 0;
	bool isInList = IsMailboxExists(login, &size);

	if (isInList)
		return EmailManagerError::MailboxExists;

	TCHAR* path = CreateFullPath(login);
	HANDLE file = CreateFile(path, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, CREATE_NEW, FILE_FLAG_WRITE_THROUGH, NULL);

	DWORD bytesWritten = 0;
	TCHAR* fileStructure = CreateFileStructure(DEFAULT_MAIL_SIZE);

	WriteFile(file, fileStructure, FULL_FORMAT_SIZE, &bytesWritten, NULL);
	SetEndOfFile(file);

	CloseHandle(file);
	free(path);
	free(fileStructure);

	return EmailManagerError::OK;
}

EmailManagerError EmailManager::SelectMailbox(TCHAR* login)
{
	if (currentMailbox != nullptr)
		return EmailManagerError::MailboxAlreadySelected;

	int size = 0;
	bool isInList = IsMailboxExists(login, &size);

	if (size == 0)
		return EmailManagerError::NoMailboxes;

	if (!isInList)
		return EmailManagerError::MailboxNotFound;

	bool isCorrupted = IsMailboxCorrupted(login);

	if (isCorrupted)
		return EmailManagerError::MailboxCorrupted;

	currentMailbox = (TCHAR*)malloc((_tcslen(login) + 1) * sizeof(TCHAR));
	_tcscpy(currentMailbox, login);

	return EmailManagerError::OK;
}

EmailManagerError EmailManager::LogoutMailbox()
{
	if (currentMailbox == nullptr)
		return EmailManagerError::MailboxNotSelected;
	free(currentMailbox);
	currentMailbox = nullptr;
	return EmailManagerError::OK;
}

EmailManagerError EmailManager::GetMailboxes(TCHAR*** names, int* size)
{
	TCHAR* szDir = (TCHAR*)malloc((_tcslen(dir_path_) + EXTENSION_SIZE + 4) * sizeof(TCHAR));
	_tcscpy(szDir, dir_path_);
	_tcscat(szDir, _T("\\*.bin*"));

	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	*size = GetNumberOfFilesInDirectory(szDir);
	int ind = 0;
	*names = (TCHAR**)malloc(*size * sizeof(TCHAR*));

	if (!*size)
	{
		free(szDir);
		return EmailManagerError::NoMailboxes;
	}

	hFind = FindFirstFile(szDir, &ffd);

	do {
		TCHAR* currentName = ffd.cFileName;
		int len = _tcslen(currentName) + 1 - EXTENSION_SIZE;
		(*names)[ind] = (TCHAR*)malloc(len * sizeof(TCHAR));
		_tcsncpy((*names)[ind], currentName, len - 1);
		(*names)[ind][len - 1] = 0;
		ind++;
	} while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);
	free(szDir);
	return EmailManagerError::OK;
}

EmailManagerError EmailManager::CreateMessage(TCHAR* destination, TCHAR* message, int* index)
{
	if (currentMailbox == nullptr)
		return EmailManagerError::MailboxNotSelected;

	// Structure of message:  32bit Index - 32bit Size - 512bit destination - Nbit message (in size)
	TCHAR* fileName = CreateFullPath(currentMailbox);
	int messageSize = (_tcslen(message) + 1) * sizeof(TCHAR),
		fullSize = FULL_MESSAGE_HEADER_SIZE + messageSize;
	DWORD written = 0;

	HANDLE headerDesc = CreateFile(fileName, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	TCHAR* header = (TCHAR*)malloc(FILE_HEADER_SIZE);
	ReadFile(headerDesc, header, FILE_HEADER_SIZE, &written, NULL);
	CloseHandle(headerDesc);

	int* intPtr = (int*)header;
	int maxSize = *(intPtr + 2);

	if (*intPtr + fullSize > maxSize){
		free(header);
		return EmailManagerError::MailboxOverflow;
	}

	TCHAR* text = (TCHAR*)malloc(fullSize);

	int* indList = nullptr;
	uint32_t count = 0;
	GetMessageList(&indList, &count);
	int newInd = 1;
	if (count != 0)
		newInd = indList[count - 1] + 1;
	*index = newInd;

	int* tempPtr = (int*)text;
	*tempPtr = newInd;
	tempPtr++;// Skip 32bit index
	*tempPtr = messageSize;
	tempPtr++;// Skip 32bit size

	TCHAR* txtPtr = (TCHAR*)tempPtr;
	_tcscpy(txtPtr, destination);
	txtPtr += 32; // Skip 512bit destination (64byte)
	_tcscpy(txtPtr, message);

	HANDLE writeDesc = CreateFile(fileName, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	SetFilePointer(writeDesc, 0, NULL, FILE_END);
	WriteFile(writeDesc, text, fullSize, &written, NULL);
	SetEndOfFile(writeDesc);
	CloseHandle(writeDesc);

	UpdateHeader(fullSize, 1);
	RecalculateChecksum();

	free(fileName);
	free(header);
	free(text);
	return EmailManagerError::OK;
}

EmailManagerError EmailManager::CreateMessage(Message * message, int * index)
{
	EmailManagerError error = CreateMessage(message->destination, message->message, index);
	return error;
}

EmailManagerError EmailManager::GetMessageList(int** messageIndexesList, uint32_t* messageCount)
{
	if (currentMailbox == nullptr)
		return EmailManagerError::MailboxNotSelected;

	TCHAR* fileName = CreateFullPath(currentMailbox);
	HANDLE file = CreateFile(fileName, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	TCHAR* header = (TCHAR*)malloc(FULL_FORMAT_SIZE);
	DWORD written = 0;

	ReadFile(file, header, FULL_FORMAT_SIZE, &written, NULL);
	int* intPtr = (int*)header;
	*messageCount = *(intPtr + 1);
	*messageIndexesList = (int*)malloc(*messageCount * sizeof(int));

	TCHAR* mesInfoBuf = (TCHAR*)malloc(FULL_MESSAGE_HEADER_SIZE);

	for (int i = 0; i < *messageCount; i++) {
		ReadFile(file, mesInfoBuf, FULL_MESSAGE_HEADER_SIZE, &written, NULL);
		intPtr = (int*)mesInfoBuf;
		(*messageIndexesList)[i] = *intPtr;
		intPtr++;
		int length = *intPtr;
		intPtr++;
		SetFilePointer(file, length, NULL, FILE_CURRENT);
	}

	CloseHandle(file);
	free(fileName);
	free(mesInfoBuf);
	free(header);
	return EmailManagerError::OK;
}

EmailManagerError EmailManager::GetMessageByIndex(int index, Message ** message)
{
	if (currentMailbox == nullptr)
		return EmailManagerError::MailboxNotSelected;

	TCHAR* fileName = CreateFullPath(currentMailbox);
	HANDLE file = CreateFile(fileName, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	TCHAR* header = (TCHAR*)malloc(FULL_FORMAT_SIZE);
	DWORD written = 0;

	ReadFile(file, header, FULL_FORMAT_SIZE, &written, NULL);
	int* intPtr = (int*)header;
	int count = *(intPtr + 1);

	TCHAR* mesHeaderBuf = (TCHAR*)malloc(FULL_MESSAGE_HEADER_SIZE);
	int length = 0;
	bool isFound = false;

	for (int i = 0; i < count; i++) {
		ReadFile(file, mesHeaderBuf, FULL_MESSAGE_HEADER_SIZE, &written, NULL);
		int* intPtr = (int*)mesHeaderBuf;
		int currInd = *intPtr;
		length = *(intPtr + 1);
		if (currInd == index) {
			isFound = true;
			break;
		}
		SetFilePointer(file, length, NULL, FILE_CURRENT);
	}

	if (!isFound) {
		return EmailManagerError::MessageNotFound;
	}
	SetFilePointer(file, -(MESSAGE_RECIPIENT_SIZE), NULL, FILE_CURRENT);

	TCHAR* messageText = (TCHAR*)malloc(length);
	TCHAR* recipient = (TCHAR*)malloc(MESSAGE_RECIPIENT_SIZE);
	ReadFile(file, recipient, MESSAGE_RECIPIENT_SIZE, &written, NULL);
	ReadFile(file, messageText, length, &written, NULL);

	*message = (Message*)malloc(sizeof Message);
	(*message)->destination = recipient;
	(*message)->message = messageText;

	CloseHandle(file);
	free(fileName);
	free(mesHeaderBuf);
	free(header);
	return EmailManagerError::OK;
}

EmailManagerError EmailManager::ReadAndDelete(int index, Message ** message)
{
	EmailManagerError readError = GetMessageByIndex(index, message);

	if (readError != EmailManagerError::OK)
		return readError;

	return DeleteMessage(index);
}

EmailManagerError EmailManager::DeleteMessage(int index)
{
	if (currentMailbox == nullptr)
		return EmailManagerError::MailboxNotSelected;

	TCHAR* fileName = CreateFullPath(currentMailbox);
	HANDLE file = CreateFile(fileName, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	TCHAR* header = (TCHAR*)malloc(FULL_FORMAT_SIZE);
	DWORD read = 0;

	ReadFile(file, header, FULL_FORMAT_SIZE, &read, NULL);
	int* intPtr = (int*)header;
	int fileSize = *intPtr;
	int count = *(intPtr + 1);

	TCHAR* mesHeaderBuf = (TCHAR*)malloc(FULL_MESSAGE_HEADER_SIZE);
	int countPrev = FULL_FORMAT_SIZE,
		delMesLength = 0;
	bool isFound = false;

	for (int i = 0; i < count; i++) {
		ReadFile(file, mesHeaderBuf, FULL_MESSAGE_HEADER_SIZE, &read, NULL);
		int* intPtr = (int*)mesHeaderBuf;
		int currInd = *intPtr,
			length = *(intPtr + 1);

		if (currInd == index) {
			delMesLength = length;
			isFound = true;
			break;
		}
		countPrev += FULL_MESSAGE_HEADER_SIZE + length;

		SetFilePointer(file, length, NULL, FILE_CURRENT);
	}

	if (!isFound) {
		return EmailManagerError::MessageNotFound;
	}
	SetFilePointer(file, 0, NULL, FILE_BEGIN);

	int newFileSize = fileSize + FULL_FORMAT_SIZE - (delMesLength + FULL_MESSAGE_HEADER_SIZE);
	TCHAR* newFileText = (TCHAR*)malloc(newFileSize);
	int countNext = newFileSize - countPrev;

	ReadFile(file, newFileText, countPrev, &read, NULL);
	SetFilePointer(file, delMesLength + FULL_MESSAGE_HEADER_SIZE, NULL, FILE_CURRENT);

	char* tmpChrPtr = (char*)newFileText;
	tmpChrPtr += countPrev;

	ReadFile(file, tmpChrPtr, countNext, &read, NULL);

	DWORD written = 0;
	SetFilePointer(file, 0, NULL, FILE_BEGIN);
	WriteFile(file, newFileText, newFileSize, &written, NULL);

	SetEndOfFile(file);
	CloseHandle(file);

	UpdateHeader(-(delMesLength + FULL_MESSAGE_HEADER_SIZE), -1);
	RecalculateChecksum();

	free(fileName);
	free(newFileText);
	free(mesHeaderBuf);
	free(header);
	return EmailManagerError::OK;
}

EmailManagerError EmailManager::IsMailboxCorrupted(TCHAR * login)
{
	TCHAR* fullPath = CreateFullPath(login);
	CRCType fileChecksum = 0;


	HANDLE file = CreateFile(fullPath, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	if (file == INVALID_HANDLE_VALUE)
	{
		return EmailManagerError::MailboxNotFound;
	}
	DWORD read;

	SetFilePointer(file, FILE_HEADER_SIZE, NULL, FILE_BEGIN);
	ReadFile(file, (void*)&fileChecksum, 1, &read, NULL);

	CloseHandle(file);

	CRCType calcChecksum = CalculateChecksum(fullPath);
	free(fullPath);

	if (fileChecksum != calcChecksum)
		return EmailManagerError::MailboxCorrupted;

	return EmailManagerError::OK;
}

void EmailManager::InitiateDirectory(TCHAR* root)
{
	TCHAR* szDir = (TCHAR*)malloc((_tcslen(root) + 4) * sizeof(TCHAR));
	_tcscpy(szDir, root);
	_tcscat(szDir, _T("Mail"));

	BOOL res = CreateDirectory(szDir, NULL);
	// res != 0 -> succeed, else -> it exists
	dir_path_ = szDir;
}

TCHAR* EmailManager::CreateFullPath(TCHAR* filename)
{
	TCHAR* fullPath = (TCHAR*)malloc((_tcslen(dir_path_) + _tcslen(filename) + 2 + EXTENSION_SIZE) * sizeof(TCHAR));
	_tcscpy(fullPath, dir_path_);
	_tcscat(fullPath, _T("\\"));
	_tcscat(fullPath, filename);
	_tcscat(fullPath, _T(".bin"));

	return fullPath;
}

TCHAR* EmailManager::AddExtension(TCHAR * login)
{
	TCHAR* fileName = (TCHAR*)malloc(_tcslen(login) + EXTENSION_SIZE + 1);
	_tcscpy(fileName, login);
	_tcscat(fileName, _T(".bin"));
	return fileName;
}

TCHAR* EmailManager::CreateFileStructure(int mailboxMaxsize) {
	int size = FILE_HEADER_SIZE + CHECKSUM_SIZE;
	TCHAR* text = (TCHAR*)malloc(size);
	int* intPtr = (int*)text;
	for (int i = 0; i < 2; i++) {
		*intPtr = 0;
		intPtr++;
	}
	*intPtr = mailboxMaxsize;
	intPtr++;
	uint8_t crc = GetCRC(text, FILE_HEADER_SIZE);
	*((uint8_t*)intPtr) = crc;
	
	return text;
}

void EmailManager::RecalculateChecksum()
{
	TCHAR* fullPath = CreateFullPath(currentMailbox);
	CRCType checksum = CalculateChecksum(fullPath);

	HANDLE file = CreateFile(fullPath, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	DWORD written;

	SetFilePointer(file, FILE_HEADER_SIZE, NULL, FILE_BEGIN);
	WriteFile(file, &checksum, 1, &written, NULL);

	CloseHandle(file);
	free(fullPath);
}

CRCType EmailManager::CalculateChecksum(TCHAR* fullPath)
{
	HANDLE file = CreateFile(fullPath, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	TCHAR* header = (TCHAR*)malloc(FILE_HEADER_SIZE);
	DWORD read;

	ReadFile(file, header, FILE_HEADER_SIZE, &read, NULL);
	int fileSize = *((int*)header) + FILE_HEADER_SIZE;
	TCHAR* text = (TCHAR*)malloc(fileSize);

	memcpy(text, header, FILE_HEADER_SIZE);
	SetFilePointer(file, 1, NULL, FILE_CURRENT);


	ReadFile(file, text + FILE_HEADER_SIZE / sizeof(TCHAR), fileSize - FILE_HEADER_SIZE, &read, NULL);
	
	CRCType checksum = GetCRC(text, fileSize);

	CloseHandle(file);
	free(header);

	return checksum;
}

bool EmailManager::IsMailboxExists(TCHAR* login, int* count)
{
	TCHAR** list = nullptr;
	GetMailboxes(&list, count);
	bool isInList = false;

	for (int i = 0; i < *count; i++) {
		if (!_tcscmp(login, list[i]))
			isInList = true;
	}

	free(list);
	return isInList;
}

void EmailManager::UpdateHeader(int size, int count)
{
	TCHAR* fileName = CreateFullPath(currentMailbox);
	HANDLE file = CreateFile(fileName, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	DWORD tempCounts = 0;
	TCHAR* header = (TCHAR*)malloc(FILE_HEADER_SIZE);
	ReadFile(file, header, FILE_HEADER_SIZE, &tempCounts, NULL);

	int* headerIntPtr = (int*)header;
	int fileSizeH = *headerIntPtr;
	int countH = *(headerIntPtr + 1);

	*headerIntPtr = fileSizeH + size;
	*(headerIntPtr + 1) = countH + count;

	SetFilePointer(file, 0, NULL, FILE_BEGIN);
	WriteFile(file, header, FILE_HEADER_SIZE, &tempCounts, NULL);

	CloseHandle(file);
	free(fileName);
	free(header);
}

int EmailManager::GetNumberOfFilesInDirectory(TCHAR* dir)
{
	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	int count = 0;

	hFind = FindFirstFile(dir, &ffd);

	if (hFind == INVALID_HANDLE_VALUE)
		return 0;

	do {
		count++;
	} while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);
	return count;
}

EmailManagerError EmailManager::ClearMailbox() {
	if (currentMailbox == nullptr)
		return EmailManagerError::MailboxNotSelected;

	TCHAR* fileName = CreateFullPath(currentMailbox);
	HANDLE file = CreateFile(fileName, GR_GW, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH, NULL);
	DWORD tempCounts = 0;
	TCHAR* header = (TCHAR*)malloc(FILE_HEADER_SIZE);

	ReadFile(file, header, FILE_HEADER_SIZE, &tempCounts, NULL);
	int maxSize = *((int*)header + 2);
	free(header);

	SetEndOfFile(file);
	header = CreateFileStructure(maxSize);
	SetFilePointer(file, 0, NULL, FILE_BEGIN);
	WriteFile(file, header, FULL_FORMAT_SIZE, &tempCounts, NULL);
	CloseHandle(file);

	return EmailManagerError::OK;
}