#define _CRT_NON_CONFORMING_WCSTOK
#define _CRT_SECURE_NO_WARNINGS
// stdafx.h: включаемый файл для стандартных системных включаемых файлов
// или включаемых файлов для конкретного проекта, которые часто используются, но
// не часто изменяются
//

#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <string.h>
#include "CRC.h"
#include <stdint.h>
#include <iostream>
#include <Windows.h>
#include "EmailManager.h"
#include "EmailManagerCLI.h"

// TODO: Установите здесь ссылки на дополнительные заголовки, требующиеся для программы

#define GR_GW GENERIC_READ | GENERIC_WRITE
#define FILE_HEADER_SIZE 12
#define CHECKSUM_SIZE 1
#define FULL_FORMAT_SIZE FILE_HEADER_SIZE + CHECKSUM_SIZE
#define EXTENSION_SIZE 4
#define MESSAGE_INFO_SIZE 8
#define MESSAGE_RECIPIENT_SIZE 64
#define FULL_MESSAGE_HEADER_SIZE MESSAGE_INFO_SIZE + MESSAGE_RECIPIENT_SIZE
#define DEFAULT_MAIL_SIZE 10000