#pragma once
#include "stdafx.h"

#ifdef UNICODE
#define _tcstok wcstok
#else
#define _tcstok strtok
#endif // UNICODE

long long _tcspos(const TCHAR* source, const TCHAR* substr, size_t offset);
size_t not_fucked_up_tcslen(const TCHAR* source, const size_t maxlen = SIZE_MAX);