#pragma once
#include "stdafx.h"
#include "IEmailManager.h"

class EmailManager : public IEmailManager
{
public:
	EmailManager();
	EmailManager(TCHAR* directoryPath);
	~EmailManager();
	EmailManagerError CreateMailbox(TCHAR * login) override; 
	EmailManagerError SelectMailbox(TCHAR* login) override; 
	EmailManagerError LogoutMailbox() override; 
	EmailManagerError GetMailboxes(__out TCHAR*** names, __out int* size) override; 

	EmailManagerError CreateMessage(TCHAR* destination, TCHAR* message, __out int* index) override; 
	EmailManagerError CreateMessage(Message* message, __out int* index) override; 

	EmailManagerError GetMessageList(__out int** messageIndexesList, __out uint32_t* messageCount) override; 
	EmailManagerError GetMessageByIndex(int index, __out Message** message) override; 
	EmailManagerError ReadAndDelete(int index, __out Message** message) override;

	EmailManagerError DeleteMessage(int index) override;
	EmailManagerError ClearMailbox() override;

	EmailManagerError IsMailboxCorrupted(TCHAR* login) override;
private:
	TCHAR* dir_path_;
	TCHAR* currentMailbox;

	void InitiateDirectory(TCHAR* root);
	TCHAR* CreateFullPath(TCHAR* filename);
	TCHAR* AddExtension(TCHAR* login);
	TCHAR* CreateFileStructure(int mailboxMaxsize);
	CRCType CalculateChecksum(TCHAR* login);
	void RecalculateChecksum();
	bool IsMailboxExists (TCHAR* login, int* count);
	void UpdateHeader(int size, int count);
	int GetNumberOfFilesInDirectory(TCHAR* dir);
};

