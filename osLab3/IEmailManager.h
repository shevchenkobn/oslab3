#pragma once
struct Message {
	TCHAR* message;
	TCHAR* destination;
};

enum EmailManagerError
{
	OK = 0,

	MailboxExists,
	NoMailboxes,
	MailboxNotFound,
	MailboxNotSelected,
	MailboxAlreadySelected,

	MailboxOverflow,
	MessageNotFound,
	MailboxCorrupted
};

class IEmailManager
{
public:
	IEmailManager();
	IEmailManager(TCHAR* directoryPath);
	virtual EmailManagerError CreateMailbox(TCHAR* login) = 0;
	virtual EmailManagerError LogoutMailbox() = 0;
	virtual EmailManagerError SelectMailbox(TCHAR* login) = 0;
	virtual EmailManagerError GetMailboxes(__out TCHAR*** names, __out int* size) = 0;

	virtual EmailManagerError CreateMessage(TCHAR* destination, TCHAR* message, __out int* index) = 0;
	virtual EmailManagerError CreateMessage(Message* message, __out int* index) = 0;

	virtual EmailManagerError GetMessageList(__out int** messageIndexesList, __out uint32_t* messageCount) = 0;
	virtual EmailManagerError GetMessageByIndex(int index, __out Message** message) = 0;
	virtual EmailManagerError ReadAndDelete(int index, __out Message** message) = 0;

	virtual EmailManagerError DeleteMessage(int index) = 0;
	virtual EmailManagerError ClearMailbox() = 0;

	virtual EmailManagerError IsMailboxCorrupted(TCHAR* login) = 0;
};

