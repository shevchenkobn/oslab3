#include "stdafx.h"
#include "string_helpers.h"

long long _tcspos(const TCHAR* source, const TCHAR* substr, size_t offset)
{
	TCHAR* matchPtr = _tcsstr((TCHAR*)source + offset, substr);
	return matchPtr ? matchPtr - source : -1;
}

size_t not_fucked_up_tcslen(const TCHAR* source, const size_t maxlen)
{
	size_t length = SIZE_MAX;
	do {
		length++;
	} while (length <= maxlen && source[length]);
	return length;
}