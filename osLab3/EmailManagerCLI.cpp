#include "stdafx.h"
#include <stdexcept>
#include "EmailManagerCLI.h"
#include "string_helpers.h"

const size_t COMMAND_BUFFER = 256;
const TCHAR NON_COMMAND_CHARS[] = _T(" \n\r\t");
const TCHAR MESSAGE_END_MARKER[] = _T("EOF");
const size_t MESSAGE_END_MARKER_LEN = _tcslen(MESSAGE_END_MARKER);

const TCHAR INPUT_MARK[] = _T(">>");
const TCHAR OUTPUT_MARK[] = _T("<<");

const TCHAR WELLCOME_PROMPT[] = _T("You entered Email Manager CLI");
const TCHAR BYE_PROMPT[] = _T("Exiting... Bye!");
const TCHAR EXIT_PROMPT[] = _T("Are you sure you want to exit? (y/n) ");
const TCHAR COMMAND_NOT_SUPPORTED_PROMPT[] = _T("Command is not supported.");
const TCHAR MAILBOX_START_PROMPT[] = _T("Mailbox \"");
const TCHAR MAILBOX_NOT_SELECTED_PROMPT[] = _T("No mailbox selected");
const TCHAR NO_LOGIN_PROMPT[] = _T("Please, provide a username");
const TCHAR REGISTER_DUPLICATE_END_PROMPT[] = _T("\" already exists");
const TCHAR REGISTER_OK_END_PROMPT[] = _T("\" is created. Login to use it.");
const TCHAR LOGIN_OK_END_PROMPT[] = _T("\" is selected");
const TCHAR LOGOUT_OK_END_PROMPT[] = _T("\" is unselected");
const TCHAR LOGIN_NOT_FOUND_END_PROMPT[] = _T("\" is not found");
const TCHAR LOGIN_SELECTED_END_PROMPT[] = _T("\" is already selected");
const TCHAR NO_MAILBOXES_PROMPT[] = _T("No mailboxes to show");
const TCHAR MAILBOXES_PROMPT[] = _T("Mailboxes: ");
const TCHAR DELETE_OK_START_PROMPT[] = _T("Message #");
const TCHAR DELETE_OK_END_PROMPT[] = _T(" was successfully deleted");
const TCHAR NO_ID_PROMPT[] = _T("Please, provide index of message to ");
const TCHAR DELETE_NO_ID_PROMPT[] = _T("delete");
const TCHAR READ_NO_ID_PROMPT[] = _T("read");
const TCHAR NO_MESSAGE_PROMPT[] = _T("No message found");
const TCHAR COUNT_OK_PROMPT[] = _T("Number of messages in mailbox: ");
const TCHAR LIST_OK_PROMPT[] = _T("List of available messages IDs: ");
const TCHAR MESSAGE_DEST_PROMPT[] = _T("To: ");
const TCHAR MESSAGE_BODY_PROMPT[] = _T("Content:");
const TCHAR MESSAGE_DELETE_DELIMETER_PROMPT[] = _T("\n***\n");
const TCHAR MAILBOX_OVERFLOW_PROMPT[] = _T("\" is out of memory!");
const TCHAR MAILBOX_SOLID_PROMPT[] = _T("\" is solid");
const TCHAR MAILBOX_CLEARED_PROMPT[] = _T("\" is cleared of messages");
const TCHAR MAILBOX_CORRUPTED_PROMPT[] = _T("\" is corrupted! Do something!");
const TCHAR MESSAGE_SAVED_PROMPT[] = _T("Message saved with index ");
const TCHAR MESSAGE_BODY_HELP_FORMAT_PROMPT[] = _T("Hint: to end message write %s, to write \"%s\" - \"\\%s\", to end with \"\\\" - \"\\\\%s\"\n");

const TCHAR REGISTER_COMMAND[] = _T("register");
const TCHAR LOGIN_COMMAND[] = _T("login");
const TCHAR CHECK_MAILBOX_COMMAND[] = _T("check");
const TCHAR LOGOUT_COMMAND[] = _T("logout");
const TCHAR GET_LIST_COMMAND[] = _T("list");
const TCHAR GET_MESSAGE_COMMAND[] = _T("read");
const TCHAR GET_MESSAGE_DELETE_FLAG_COMMAND[] = _T("--delete");
const TCHAR NEW_MESSAGE_COMMAND[] = _T("write");
const TCHAR DELETE_COMMAND[] = _T("delete");
const TCHAR DELETE_ALL_FLAG_COMMAND[] = _T("--all");
const TCHAR LIST_MAILBOXES_COMMAND[] = _T("mailboxes");
const TCHAR EXIT_COMMAND[] = _T("exit");

EmailManagerCLI::EmailManagerCLI(IEmailManager* emailManager, FILE* inStream, FILE* outStream)
{
	if (!emailManager)
	{
		throw std::invalid_argument("emailManager is null");
	}
	_emailManager = emailManager;

	_inStream = inStream ? inStream : stdin;
	_outStream = outStream ? outStream : stdout;
}

void EmailManagerCLI::CaptureStreams()
{
	_ftprintf(_outStream, _T("%s\n"), WELLCOME_PROMPT);
	_fromConsole = new TCHAR[COMMAND_BUFFER];
	_captured = true;
	while (_captured)
	{
		if (_userName)
		{
			_ftprintf(_outStream, _T("@%s "), _userName);
		}
		_ftprintf(_outStream, _T("%s "), INPUT_MARK);
		_fgetts(_fromConsole, COMMAND_BUFFER, _inStream);
		fseek(_inStream, 0, SEEK_END);

		TCHAR* command = _tcstok(_fromConsole, NON_COMMAND_CHARS);
		TCHAR** args = new TCHAR*[COMMAND_BUFFER / 2 + 1];
		size_t i = 0;
		args[0] = _tcstok(NULL, NON_COMMAND_CHARS);
		while (args[i])
		{
			args[++i] = _tcstok(NULL, NON_COMMAND_CHARS);
		}

		_ftprintf(_outStream, _T("%s "), OUTPUT_MARK);
		if (!_tcscmp(REGISTER_COMMAND, command))
		{
			registerAction(args, i);
		}
		else if (!_tcscmp(LOGIN_COMMAND, command))
		{
			loginAction(args, i);
		}
		else if (!_tcscmp(LIST_MAILBOXES_COMMAND, command))
		{
			getMailboxesAction(args, i);
		}
		else if (!_tcscmp(GET_LIST_COMMAND, command))
		{
			getMessageListAction(args, i);
		}
		else if (!_tcscmp(GET_MESSAGE_COMMAND, command))
		{
			getMessageAction(args, i);
		}
		else if (!_tcscmp(NEW_MESSAGE_COMMAND, command))
		{
			createMessageAction(args, i);
		}
		else if (!_tcscmp(DELETE_COMMAND, command))
		{
			deleteMessagesAction(args, i);
		}
		else if (!_tcscmp(LOGOUT_COMMAND, command))
		{
			logoutAction(args, i);
		}
		else if (!_tcscmp(CHECK_MAILBOX_COMMAND, command))
		{
			checkMailboxAction(args, i);
		}
		else if (!_tcscmp(EXIT_COMMAND, command))
		{
			exitAction(args, i);
		}
		else
		{
			_ftprintf(_outStream, _T("%s\n"), COMMAND_NOT_SUPPORTED_PROMPT);
		}

		_ftprintf(_outStream, _T("\n"));
		delete[] args;
	}
}

void EmailManagerCLI::registerAction(TCHAR** argv, size_t argc)
{
	if (!argc) {
		_ftprintf(_outStream, _T("%s\n"), NO_LOGIN_PROMPT);
		return;
	}
	EmailManagerError error = _emailManager->CreateMailbox(argv[0]);

	switch (error)
	{
	case MailboxExists:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], REGISTER_DUPLICATE_END_PROMPT);
		break;
	case OK:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], REGISTER_OK_END_PROMPT);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::getMailboxesAction(TCHAR** argv, size_t argc)
{
	int count = 0;
	TCHAR** mailboxes = NULL;
	EmailManagerError error = _emailManager->GetMailboxes(&mailboxes, &count);

	switch (error)
	{
	case NoMailboxes:
		_ftprintf(_outStream, _T("%s\n"), NO_MAILBOXES_PROMPT);
		break;
	case OK:
		_ftprintf(_outStream, _T("%s\n\n"), MAILBOXES_PROMPT);
		for (int i = 0; i < count; i++)
		{
			_ftprintf(_outStream, _T("%d. \"%s\";\n"), i + 1, mailboxes[i]);
			free(mailboxes[i]);
		}
		_ftprintf(_outStream, _T("\n"));
		free(mailboxes);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::checkMailboxAction(TCHAR** argv, size_t argc)
{
	if (!argc) {
		_ftprintf(_outStream, _T("%s\n"), NO_LOGIN_PROMPT);
		return;
	}
	EmailManagerError error = _emailManager->IsMailboxCorrupted(argv[0]);
	
	switch (error)
	{
	case OK:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], MAILBOX_SOLID_PROMPT);
		break;
	case MailboxNotFound:
	case NoMailboxes:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], LOGIN_NOT_FOUND_END_PROMPT);
		break;
	case MailboxCorrupted:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, _userName, MAILBOX_CORRUPTED_PROMPT);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::loginAction(TCHAR** argv, size_t argc)
{
	if (!argc) {
		_ftprintf(_outStream, _T("%s\n"), NO_LOGIN_PROMPT);
		return;
	}
	EmailManagerError error = _emailManager->SelectMailbox(argv[0]);
	size_t length;
	switch (error)
	{
	case OK:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], LOGIN_OK_END_PROMPT);
		length = _tcslen(argv[0]);
		_userName = new TCHAR[length + 1];
		_tcscpy(_userName, argv[0]);
		_userName[length] = 0;
		break;
	case MailboxNotFound:
	case NoMailboxes:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], LOGIN_NOT_FOUND_END_PROMPT);
		break;
	case MailboxAlreadySelected:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], LOGIN_SELECTED_END_PROMPT);
		break;
	case MailboxCorrupted:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, argv[0], MAILBOX_CORRUPTED_PROMPT);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::logoutAction(TCHAR** argv, size_t argc)
{
	EmailManagerError error = _emailManager->LogoutMailbox();

	switch (error)
	{
	case OK:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, _userName, LOGOUT_OK_END_PROMPT);
		delete _userName;
		_userName = NULL;
		break;
	case MailboxNotSelected:
		_ftprintf(_outStream, _T("%s\n"), MAILBOX_NOT_SELECTED_PROMPT);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::getMessageListAction(TCHAR** argv, size_t argc)
{
	int *list = NULL;
	uint32_t count = 0;
	EmailManagerError error = _emailManager->GetMessageList(&list, &count);

	switch (error)
	{
	case OK:
		_ftprintf(_outStream, _T("%s %d\n"), COUNT_OK_PROMPT, count);
		if (count) {
			_ftprintf(_outStream, _T("%s\n"), LIST_OK_PROMPT);
			for (size_t i = 0; i < count; i++)
			{
				_ftprintf(_outStream, _T("%d;\n"), list[i]);
			}
		}
		free(list);
		break;
	case MailboxNotSelected:
		_ftprintf(_outStream, _T("%s\n"), MAILBOX_NOT_SELECTED_PROMPT);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::getMessageAction(TCHAR** argv, size_t argc)
{
	if (!argc) {
		_ftprintf(_outStream, _T("%s%s\n"), NO_ID_PROMPT, READ_NO_ID_PROMPT);
		return;
	}
	int index = _ttoi(argv[0]);
	Message* msg = NULL;
	EmailManagerError error = _emailManager->GetMessageByIndex(index, &msg);

	switch (error)
	{
	case OK:
		_ftprintf(_outStream, _T("%s %s\n\n"), MESSAGE_DEST_PROMPT, msg->destination);
		_ftprintf(_outStream, _T("%s\n%s\n"), MESSAGE_BODY_PROMPT, msg->message);
		free(msg);
		if (argc > 1 && !_tcscmp(argv[1], GET_MESSAGE_DELETE_FLAG_COMMAND)) {
			_ftprintf(_outStream, _T("%s"), MESSAGE_DELETE_DELIMETER_PROMPT);
			deleteMessageAction(index);
		}
		break;
	case MailboxNotSelected:
		_ftprintf(_outStream, _T("%s\n"), MAILBOX_NOT_SELECTED_PROMPT);
		break;
	case MessageNotFound:
		_ftprintf(_outStream, _T("%s\n"), NO_MESSAGE_PROMPT);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::createMessageAction(TCHAR** argv, size_t argc)
{
	size_t length = 0;
	do
	{
		_ftprintf(_outStream, _T("%s "), MESSAGE_DEST_PROMPT);
		_fgetts(_fromConsole, COMMAND_BUFFER, _inStream);
		fseek(_inStream, 0, SEEK_END);
		length = _tcslen(_fromConsole);
	} while (length < 2);
	if (_fromConsole[length - 1] == _T('\n'))
	{
		length--;
	}
	TCHAR* destination = (TCHAR*)malloc((length + 1) * sizeof TCHAR);
	_tcsncpy(destination, _fromConsole, length);
	destination[length] = 0;
	length = 0;

	size_t contentLength = length;
	TCHAR* content = NULL;
	_ftprintf(
		_outStream,
		MESSAGE_BODY_HELP_FORMAT_PROMPT,
		MESSAGE_END_MARKER,
		MESSAGE_END_MARKER,
		MESSAGE_END_MARKER, 
		MESSAGE_END_MARKER
	);
	do
	{
		_ftprintf(_outStream, _T("%s\n"), MESSAGE_BODY_PROMPT);
		bool theEndIsNow = false;
		do {
			long long index = -1;
			size_t offset = 0;
			_fgetts(_fromConsole, COMMAND_BUFFER, _inStream);
			do {
				index = _tcspos(_fromConsole, MESSAGE_END_MARKER, offset);
				if (index >= 0)
				{
					if (!index || _fromConsole[index - 1] != '\\' || index > 1 && _fromConsole[index - 2] == '\\')
					{
						if (index > 1 && _fromConsole[index - 2] == '\\')
						{
							index--;
						}
						length = index - offset;
						content = (TCHAR*)realloc(content, (contentLength + length + 1) * sizeof TCHAR);
						_tcsncpy(&content[contentLength], &_fromConsole[offset], length);
						contentLength += length;
						content[contentLength] = 0;
						theEndIsNow = true;
						break;
					}
					else
					{ 
						length = index > offset ? index - offset - 1 : 0;
						content = (TCHAR*)realloc(content, (contentLength + length + MESSAGE_END_MARKER_LEN + 1) * sizeof TCHAR);
						_tcsncpy(&content[contentLength], &_fromConsole[offset], length);
						contentLength += length;//< 0 ? 0 : index - offset;
						_tcscpy(&content[contentLength], MESSAGE_END_MARKER);
						contentLength += MESSAGE_END_MARKER_LEN;
						content[contentLength] = 0;
						offset += length + MESSAGE_END_MARKER_LEN + 1;
					}
				}
				else
				{
					length = _tcslen(&_fromConsole[offset]);
					content = (TCHAR*)realloc(content, (contentLength + length + 1) * sizeof TCHAR);
					_tcscpy(&content[contentLength], &_fromConsole[offset]);
					contentLength += length;
					content[contentLength] = 0;
				}
			} while (index >= 0);
		} while (!theEndIsNow);
		fseek(_inStream, 0, SEEK_END);
	} while (!contentLength);

	int index = 0;
	EmailManagerError error = _emailManager->CreateMessage(destination, content, &index);

	_ftprintf(_outStream, _T("\n"));
	switch (error)
	{
	case OK:
		_ftprintf(_outStream, _T("%s%d\n"), MESSAGE_SAVED_PROMPT, index);
		break;
	case MailboxNotSelected:
		_ftprintf(_outStream, _T("%s\n"), MAILBOX_NOT_SELECTED_PROMPT);
		break;
	case MailboxOverflow:
		_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, _userName, MAILBOX_OVERFLOW_PROMPT);
		break;
	default:
		break;
	}
	free(destination);
	free(content);
}

void EmailManagerCLI::deleteMessagesAction(TCHAR** argv, size_t argc)
{
	if (!argc) {
		_ftprintf(_outStream, _T("%s%s\n"), NO_ID_PROMPT, DELETE_NO_ID_PROMPT);
		return;
	}
	if (!_tcscmp(argv[0], DELETE_ALL_FLAG_COMMAND))
	{
		EmailManagerError error = _emailManager->ClearMailbox();
		switch (error)
		{
		case OK:
			_ftprintf(_outStream, _T("%s%s%s\n"), MAILBOX_START_PROMPT, _userName, MAILBOX_CLEARED_PROMPT);
			break;
		case MailboxNotSelected:
			_ftprintf(_outStream, _T("%s\n"), MAILBOX_NOT_SELECTED_PROMPT);
			break;
		default:
			break;
		}
	}
	else
	{
		int index = _ttoi(argv[0]);
		deleteMessageAction(index);
	}
}

void EmailManagerCLI::deleteMessageAction(int index)
{
	EmailManagerError error = _emailManager->DeleteMessage(index);

	switch (error)
	{
	case OK:
		_ftprintf(_outStream, _T("%s%d%s\n"), DELETE_OK_START_PROMPT, index, DELETE_OK_END_PROMPT);
		break;
	case MailboxNotSelected:
		_ftprintf(_outStream, _T("%s\n"), MAILBOX_NOT_SELECTED_PROMPT);
		break;
	case MessageNotFound:
		_ftprintf(_outStream, _T("%s\n"), NO_MESSAGE_PROMPT);
		break;
	default:
		break;
	}
}

void EmailManagerCLI::exitAction(TCHAR** argv, size_t argc)
{
	do
	{
		_ftprintf(_outStream, _T("%s "), EXIT_PROMPT);
		_ftprintf(_outStream, _T("%s "), INPUT_MARK);
		_fgetts(_fromConsole, COMMAND_BUFFER, _inStream);
		fseek(_inStream, 0, SEEK_END);
	}
	while (_fromConsole[0] != _T('y') && _fromConsole[0] != _T('n'));
	if (_fromConsole[0] == _T('y'))
	{
		ReleaseStreams();
	}
}

void EmailManagerCLI::ReleaseStreams()
{
	if (_fromConsole)
	{
		delete _fromConsole;
		_fromConsole = NULL;
	}
	_captured = false;
	fseek(_inStream, 0, SEEK_END);
	_ftprintf(_outStream, _T("%s\n"), BYE_PROMPT);
}

EmailManagerCLI::~EmailManagerCLI()
{
	ReleaseStreams();
}
