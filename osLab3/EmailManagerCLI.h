#pragma once
#include "IEmailManager.h";

class EmailManagerCLI
{
protected:
	FILE* _inStream;
	FILE* _outStream;
	IEmailManager* _emailManager;
	TCHAR* _fromConsole = NULL;
	bool _captured = false;
	TCHAR* _userName = NULL;

	void registerAction(TCHAR** argv, size_t argc);
	void loginAction(TCHAR** argv, size_t argc);
	void checkMailboxAction(TCHAR** argv, size_t argc);
	void logoutAction(TCHAR** argv, size_t argc);
	void getMailboxesAction(TCHAR** argv, size_t argc);
	void getMessageListAction(TCHAR** argv, size_t argc);
	void getMessageAction(TCHAR** argv, size_t argc);
	void createMessageAction(TCHAR** argv, size_t argc);
	void deleteMessagesAction(TCHAR** argv, size_t argc);
	void deleteMessageAction(int index);
	void exitAction(TCHAR** argv, size_t argc);

public:
	EmailManagerCLI(IEmailManager* emailManager, FILE* inStream = NULL, FILE* outStream = NULL);
	~EmailManagerCLI();

	void CaptureStreams();
	void ReleaseStreams();
};

