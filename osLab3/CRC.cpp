#include "stdafx.h"
#include "CRC.h"

static CRCType crcTable[256];
static bool isInit = false;

void CRCInit() {
	CRCType  remainder;
	/*
	* Compute the remainder of each possible dividend.
	*/
	for (int dividend = 0; dividend < 256; ++dividend)
	{
		/*
		* Start with the dividend followed by zeros.
		*/
		remainder = dividend << (WIDTH - 8);

		/*
		* Perform modulo-2 division, a bit at a time.
		*/
		for (uint8_t bit = 8; bit > 0; --bit)
		{
			/*
			* Try to divide the current data bit.
			*/
			if (remainder & TOPBIT)
			{
				remainder = (remainder << 1) ^ CRC_8_CCITT;
			}
			else
			{
				remainder = (remainder << 1);
			}
		}
		/*
		* Store the result into the table.
		*/
		crcTable[dividend] = remainder;
	}
}


CRCType GetCRC(void* input, int sizeInBytes)
{
	if (!isInit) {
		CRCInit();
		isInit = true;
	}

	uint8_t* bytes = (uint8_t*)input;
	CRCType data = 0;
	CRCType remainder = 0;
	/*
	* Divide the message by the polynomial, a byte at a time.
	*/
	for (int byte = 0; byte < sizeInBytes; ++byte)
	{
		data = bytes[byte] ^ (remainder >> (WIDTH - 8));
		remainder = crcTable[data] ^ (remainder << 8);
	}
	/*
	* The final remainder is the CRC.
	*/
	return remainder;

}